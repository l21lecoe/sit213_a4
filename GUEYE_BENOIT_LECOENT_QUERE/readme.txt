Projet SIT 213
Auteurs : Eliott Quéré et Lise Le Coënt
Date : 08/09/2022

CONTENU DU DOSSIER
------------------

# les dossiers #
1. bin : contient les fichiers ".class" à éxécuter
2. docs : contient la javadoc
3. src : contient les fichier ".java" à compiler

# les fichiers #
1. cleanAll.sh : pour effacer des fichiers et dossiers dans les répertoires bin et docs
2. compile.sh : pour compiler tous les fichiers ".java" et mettre leur équivalent ".class" dans le répertoire bin
3. genDeliverable.sh : pour générer une archive livrable 
4. genDoc.sh : pour générer le javadoc
5. readme.txt : ce fichier même
6. runTests.sh : pour tester le simulateur
7. simulateur.sh : pour lancer une simulation


EXPLICATIONS
------------

les scripts bash sont la pour manipuler les fichiers dans les répertoires src et bin. C'est eux qui vont permettre de tester le simulateur avec différents paramètres d'entré.
