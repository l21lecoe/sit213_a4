package transmetteurs;

import information.Information;
import information.InformationNonConformeException;

public class EmetteurRZ<Boolean, Float> extends Emetteur<Boolean, Float>{
    Information<java.lang.Float> signalEchantillonee;
    private float v;
    private int nbTechantillon;

    public EmetteurRZ(float v, int nbTechantillon)
    {
        this.v = v;
        this.nbTechantillon = nbTechantillon;
    }

    public void recevoir(Information<Boolean> information) throws InformationNonConformeException {
        super.recevoir(information);
        this.conversion();
        this.emettre();
    }

    @Override
    public void conversion() {
        this.signalEchantillonee = new Information<>();
        for(Boolean bit : informationRecue)
        {
            if(bit.equals(true))
            {
                for(int i=0; i<nbTechantillon/3; i++) this.signalEchantillonee.add(0f);
                for(int i=0; i<nbTechantillon/3; i++) this.signalEchantillonee.add(v);
                for(int i=0; i<nbTechantillon/3; i++) this.signalEchantillonee.add(0f);
            }
            else
            {
                for(int i=0; i<nbTechantillon; i++) this.signalEchantillonee.add(0f);
            }
            informationEmise = (Information<Float>) signalEchantillonee;
        }
    }
}