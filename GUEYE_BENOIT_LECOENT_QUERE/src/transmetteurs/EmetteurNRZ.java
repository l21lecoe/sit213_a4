package transmetteurs;

import information.Information;
import information.InformationNonConformeException;

public class EmetteurNRZ<Boolean, Float> extends Emetteur<Boolean, Float>{
    Information<java.lang.Float> signalEchantillonee;
    private float vmax;
    private float vmin;
    private int nbTechantillon;

    public EmetteurNRZ(float vmax, float vmin, int nbTechantillon)
    {
        this.vmax = vmax;
        this.vmin = vmin;
        this.nbTechantillon = nbTechantillon;
    }

    public void recevoir(Information<Boolean> information) throws InformationNonConformeException{
        super.recevoir(information);
        this.conversion();
        this.emettre();
    }

    @Override
    public void conversion() {
        this.signalEchantillonee = new Information<java.lang.Float>();
        for(Boolean bit : informationRecue)
        {
            for(int i=0; i<nbTechantillon; i++)
            {
                if(bit.equals(true)) signalEchantillonee.add(vmax);
                else signalEchantillonee.add(vmin);
            }
            informationEmise = (Information<Float>) this.signalEchantillonee;

        }
    }
}