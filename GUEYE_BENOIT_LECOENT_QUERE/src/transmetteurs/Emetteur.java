package transmetteurs;

import information.Information;
import information.InformationNonConformeException;

public abstract class Emetteur<Boolean, Float> extends Transmetteur<Boolean, Float>{

    @Override
    public void recevoir(Information<Boolean> information) throws InformationNonConformeException {
        this.informationRecue = information;
    }

    @Override
    public void emettre() throws InformationNonConformeException {

    }

    public abstract void conversion();
}
